!calculates the derivatives of KS valence energies and wave-functions
SUBROUTINE make_derivative(ia,ider,delta, psi0,psi1 )

  USE kinds, ONLY : DP
  USE bse_basic_structures
  USE force_basic_structures
  USE ions_base, ONLY : tau,nat
  USE cell_base, ONLY : alat
  USE extrapolation,        ONLY :  extrapolate_charge
  USE control_flags,  ONLY : tr2,niter,mixing_beta
  
  implicit none
  
  INTEGER :: ia!ATOM TO BE MOVED
  INTEGER :: ider!CARTESIAN DIRECTION OF MOVEMENT
  INTEGER :: delta! VALUE OF +/- DISPLACEMENT
  TYPE(v_state) :: psi0!valence wavefunctions at equilibrium
  TYPE(v_state_der) :: psi1!derivative of wavefunctions

  REAL(kind=DP), ALLOCATABLE :: tau_saved(:,:)
  CHARACTER(LEN=256)    :: dirname
  
  !set threshold for selfconsistency
  tr2=1d-10
  niter=100
  mixing_beta=0.5
  
  !save a copy of coordinates
  allocate(tau_saved(3,nat))
  tau_saved(1:3,1:nat)=tau(1:3,1:nat)

  !adjust coordinates according to displacement
  tau(ider,ia)=tau(ider,ia)+delta/alat
  
   !
  ! ... update the wavefunctions, charge density, potential
  ! ... update_pot initializes structure factor array as well
  !
  !CALL update_pot()
  call extrapolate_charge( dirname, 0 )  
  !
  ! ... re-initialize atomic position-dependent quantities
  !
  CALL hinit1()
  !solve DFT scf equation

  call electrons()

  !in case rotate wavefunctions

  !calculate derivates

  !restore equilibrium coordinate
  tau(1:3,1:nat)=tau_saved(1:3,1:nat)

  deallocate(tau_saved)
  return
  
END SUBROUTINE make_derivative
