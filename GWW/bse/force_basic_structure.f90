  !This module contains objects for the derivative quantities


MODULE force_basic_structures

  USE kinds, ONLY : DP

  TYPE v_state_der
     !this contains the derivatives of the valence wave-functions and the derivatives of
     !KS eigen-energies

     INTEGER :: nspin  ! number of spin channels
     INTEGER :: numb_v(2) ! number valence state
     INTEGER :: npw ! number of g-vectors per processor
     REAL(kind=DP), DIMENSION(:,:), POINTER :: et_der(:,:)! derivatives of eigen-energies (max(numb_v),nspin)
     COMPLEX(kind=DP), DIMENSION(:,:,:), POINTER :: wfn_der!derivative of KS wavefunction (npw,numb_v,nspin)
     INTEGER :: gstart
     
     
  END type v_state_der


CONTAINS

  



  subroutine initialize_v_state_der(v_der)
    implicit none
    type(v_state_der) :: v_der
    nullify(v_der%wfn_der)
    nullify(v_der%et_der)
    return
  end subroutine initialize_v_state_der

  subroutine free_v_state_der(v_der)
    implicit none
    type(v_state_der) :: v_der
    if(associated(v_der%wfn_der)) deallocate(v_der%wfn_der)
    if(associated(v_der%et_der)) deallocate(v_der%et_der)
    return
  end subroutine free_v_state_der


 





END MODULE force_basic_structures
